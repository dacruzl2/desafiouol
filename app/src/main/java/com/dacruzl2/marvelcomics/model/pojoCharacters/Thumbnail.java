
package com.dacruzl2.marvelcomics.model.pojoCharacters;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

import androidx.room.ColumnInfo;

public class Thumbnail implements Parcelable {

    @Expose
    private String extension;
    @Expose
    private String path;

    @ColumnInfo(name = "url_thumbnail")
    private String url;
    public String getUrl() {
        return getPath().concat(".") + getExtension();
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.extension);
        dest.writeString(this.path);
    }

    public Thumbnail() {
    }

    protected Thumbnail(Parcel in) {
        this.extension = in.readString();
        this.path = in.readString();
    }

    public static final Parcelable.Creator<Thumbnail> CREATOR = new Parcelable.Creator<Thumbnail>() {
        @Override
        public Thumbnail createFromParcel(Parcel source) {
            return new Thumbnail(source);
        }

        @Override
        public Thumbnail[] newArray(int size) {
            return new Thumbnail[size];
        }
    };
}
