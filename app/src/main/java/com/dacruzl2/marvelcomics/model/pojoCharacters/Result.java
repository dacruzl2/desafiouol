
package com.dacruzl2.marvelcomics.model.pojoCharacters;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity (tableName = "result_characters")
public class Result implements Parcelable {

    //@Expose
    //private Comics comics;



    private String error = "No Character Description";

    public void setError(String error) {
        this.error = error;
    }


    @Expose
    @ColumnInfo(name = "description")
    private String description;
    //@Expose
   // private Events events;
    @Expose
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private Long id;
    @Expose
    @ColumnInfo(name = "modified")
    private String modified;
    @Expose
    @ColumnInfo(name = "name")
    private String name;
    @Expose
    @ColumnInfo(name = "resourceURI")
    private String resourceURI;
    //@Expose
    //private Series series;
    //@Expose
   // private Stories stories;
    @Expose
   // @ColumnInfo(name = "thumbnail")
   // @TypeConverters(RoomTypeConverters.class)
    @Embedded
    private Thumbnail thumbnail;
    //@Expose
    //private List<Url> urls;

    public String getError() {
        return error;
    }

    //public Comics getComics() {
      //  return comics;
    //}

    //public void setComics(Comics comics) {
      //  this.comics = comics;
    //}

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

/*
    public Events getEvents() {
        return events;
    }
*/

    /*public void setEvents(Events events) {
        this.events = events;
    }*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getResourceURI() {
        return resourceURI;
    }

    public void setResourceURI(String resourceURI) {
        this.resourceURI = resourceURI;
    }

/*
    public Series getSeries() {
        return series;
    }
*/

/*
    public void setSeries(Series series) {
        this.series = series;
    }
*/

/*
    public Stories getStories() {
        return stories;
    }
*/

/*
    public void setStories(Stories stories) {
        this.stories = stories;
    }
*/

    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

/*    public List<Url> getUrls() {
        return urls;
    }

    public void setUrls(List<Url> urls) {
        this.urls = urls;
    }*/

    public Result() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.error);
        dest.writeString(this.description);
        dest.writeValue(this.id);
        dest.writeString(this.modified);
        dest.writeString(this.name);
        dest.writeString(this.resourceURI);
        dest.writeParcelable(this.thumbnail, flags);
    }

    protected Result(Parcel in) {
        this.error = in.readString();
        this.description = in.readString();
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.modified = in.readString();
        this.name = in.readString();
        this.resourceURI = in.readString();
        this.thumbnail = in.readParcelable(Thumbnail.class.getClassLoader());
    }

    public static final Creator<Result> CREATOR = new Creator<Result>() {
        @Override
        public Result createFromParcel(Parcel source) {
            return new Result(source);
        }

        @Override
        public Result[] newArray(int size) {
            return new Result[size];
        }
    };
}
