package com.dacruzl2.marvelcomics.model;

import com.dacruzl2.marvelcomics.model.pojoCharacters.Result;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface MarvelDAO {

    @Query("SELECT * FROM result_characters")
    public List<Result> getAllResultsCharacters();

   /* @Query("SELECT * FROM Thumbnails")
    public List<Thumbnail> getAllThumbnails();
*/
    @Query("SELECT * FROM result_characters WHERE modified LIKE :modified")
    public List<Result> getCharactersByModified(String modified);

    @Insert(onConflict = REPLACE)
    public void insert(List<Result> result);

   // public void insert(Thumbnail thumbnail);

    @Update
    public void update(Result result);

    //public void update(Thumbnail thumbnail);

    @Delete
    public void delete(Result result);
   // public void delete(Thumbnail thumbnail);
}
