
package com.dacruzl2.marvelcomics.model.pojoCharacters;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import com.google.gson.annotations.Expose;

public class Data implements Parcelable {

    @Expose
    private Long count;
    @Expose
    private Long limit;
    @Expose
    private Long offset;
    @Expose
    private List<Result> results;
    @Expose
    private Long total;

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Long getLimit() {
        return limit;
    }

    public void setLimit(Long limit) {
        this.limit = limit;
    }

    public Long getOffset() {
        return offset;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.count);
        dest.writeValue(this.limit);
        dest.writeValue(this.offset);
        dest.writeTypedList(this.results);
        dest.writeValue(this.total);
    }

    public Data() {
    }

    protected Data(Parcel in) {
        this.count = (Long) in.readValue(Long.class.getClassLoader());
        this.limit = (Long) in.readValue(Long.class.getClassLoader());
        this.offset = (Long) in.readValue(Long.class.getClassLoader());
        this.results = in.createTypedArrayList(Result.CREATOR);
        this.total = (Long) in.readValue(Long.class.getClassLoader());
    }

    public static final Parcelable.Creator<Data> CREATOR = new Parcelable.Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel source) {
            return new Data(source);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };
}
