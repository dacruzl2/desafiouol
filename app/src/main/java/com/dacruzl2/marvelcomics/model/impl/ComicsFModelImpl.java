package com.dacruzl2.marvelcomics.model.impl;


import com.dacruzl2.marvelcomics.network.MarvelService;
import com.dacruzl2.marvelcomics.model.inter.IComicsFModel;
import com.dacruzl2.marvelcomics.model.pojoComics.RootComics;

import retrofit2.Call;

public class ComicsFModelImpl implements IComicsFModel {
    MarvelService marvelService;

    @Override
    public Call<RootComics> retrieveComics(int limit, String apikey, String hash, long ts) {
        return marvelService.retrieveComics(limit, apikey, ts, hash);
    }
}
