package com.dacruzl2.marvelcomics.model.inter;

import com.dacruzl2.marvelcomics.model.pojoComics.RootComics;

import retrofit2.Call;

public interface IComicsFModel{

    Call<RootComics> retrieveComics(int limit, String apikey,String hash, long ts);


}
