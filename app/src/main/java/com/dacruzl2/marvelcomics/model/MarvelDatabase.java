package com.dacruzl2.marvelcomics.model;

import android.content.Context;

import com.dacruzl2.marvelcomics.model.pojoCharacters.Result;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;


@Database(entities = {Result.class}, version = 3, exportSchema = false)
@TypeConverters({RoomTypeConverters.class})
public abstract class MarvelDatabase extends RoomDatabase {

    private static final Migration MIGRATION_1_2 = new Migration(1,2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE result_characters ADD COLUMN hiredDay INTEGER");
        }
    };



    private static MarvelDatabase marvelDatabase;

    public abstract MarvelDAO createMarvelDAO();

    public static MarvelDatabase getInstance(Context context) {
        if (marvelDatabase == null) {
            marvelDatabase = Room.databaseBuilder(context.getApplicationContext(),
                    MarvelDatabase.class, "marvel_database")
                    .fallbackToDestructiveMigration()
                   // .addMigrations(MIGRATION_1_2)
                    .build();
        }

        return marvelDatabase;
    }

}
