
package com.dacruzl2.marvelcomics.model.pojoCharacters;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

public class Root implements Parcelable {

    @Expose
    private String attributionHTML;
    @Expose
    private String attributionText;
    @Expose
    private Long code;
    @Expose
    private String copyright;
    @Expose
    private Data data;
    @Expose
    private String etag;
    @Expose
    private String status;

    public String getAttributionHTML() {
        return attributionHTML;
    }

    public void setAttributionHTML(String attributionHTML) {
        this.attributionHTML = attributionHTML;
    }

    public String getAttributionText() {
        return attributionText;
    }

    public void setAttributionText(String attributionText) {
        this.attributionText = attributionText;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.attributionHTML);
        dest.writeString(this.attributionText);
        dest.writeValue(this.code);
        dest.writeString(this.copyright);
        dest.writeParcelable(this.data, flags);
        dest.writeString(this.etag);
        dest.writeString(this.status);
    }

    public Root() {
    }

    protected Root(Parcel in) {
        this.attributionHTML = in.readString();
        this.attributionText = in.readString();
        this.code = (Long) in.readValue(Long.class.getClassLoader());
        this.copyright = in.readString();
        this.data = in.readParcelable(Data.class.getClassLoader());
        this.etag = in.readString();
        this.status = in.readString();
    }

    public static final Parcelable.Creator<Root> CREATOR = new Parcelable.Creator<Root>() {
        @Override
        public Root createFromParcel(Parcel source) {
            return new Root(source);
        }

        @Override
        public Root[] newArray(int size) {
            return new Root[size];
        }
    };
}
