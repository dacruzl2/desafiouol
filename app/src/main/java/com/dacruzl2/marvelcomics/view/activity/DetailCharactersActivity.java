package com.dacruzl2.marvelcomics.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dacruzl2.marvelcomics.R;
import com.dacruzl2.marvelcomics.model.pojoCharacters.Root;
import com.dacruzl2.marvelcomics.presenter.impl.DetailCharactersAPresenterImpl;
import com.dacruzl2.marvelcomics.presenter.inter.IDetailCharactersAPresenter;
import com.dacruzl2.marvelcomics.view.inter.IDetailCharactersAView;


import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import id.voela.actrans.AcTrans;

public class DetailCharactersActivity extends AppCompatActivity implements IDetailCharactersAView {

    private IDetailCharactersAPresenter mIDetailCharactersAPresenter;

    TextView tvDefineDescription, tvDescription, tvCharacterTeste;
    ImageView ivCharacterDetail;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIDetailCharactersAPresenter = new DetailCharactersAPresenterImpl(this);
        setContentView(R.layout.activity_detail_characters);
        supportPostponeEnterTransition();


        tvDescription = findViewById(R.id.tvDescription);
        tvDefineDescription = findViewById(R.id.tvDefineDescription);
        ivCharacterDetail = findViewById(R.id.ivCharacterDetail);
        tvCharacterTeste = findViewById(R.id.tvCharacterTeste);

        Intent it = getIntent();
        String descriptionCharacter = it.getStringExtra("descriptionCharacter");
        String imageCharacter = it.getStringExtra("imagem");
        String nameCharacter = it.getStringExtra("CharacterName");

        tvCharacterTeste.setText(nameCharacter);

        tvDefineDescription.setText(descriptionCharacter);

        Glide.with(this)
                .load(imageCharacter)
                .circleCrop()
                .into(ivCharacterDetail);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        new AcTrans.Builder(this).performFade();
    }
}
