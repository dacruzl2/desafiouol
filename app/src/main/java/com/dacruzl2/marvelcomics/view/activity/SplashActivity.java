package com.dacruzl2.marvelcomics.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.dacruzl2.marvelcomics.R;
import androidx.appcompat.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {

    TextView tvAutor;
    ImageView ivLogoMarvel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        tvAutor = findViewById(R.id.tvNomeAutor);
        ivLogoMarvel = findViewById(R.id.ivLogoMarvel);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.mytransition);
        animation.setDuration(3000);
        tvAutor.startAnimation(animation);
        ivLogoMarvel.startAnimation(animation);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                abrirAutenticacao();
            }

        }, 5000);
    }

    private void abrirAutenticacao() {

        Intent intent = new Intent(SplashActivity.this, WelcomeActivity.class);
        startActivity(intent);
    }
}

   /* public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }*/
