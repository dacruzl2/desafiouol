package com.dacruzl2.marvelcomics.view.activity;

import android.os.Bundle;

import com.dacruzl2.marvelcomics.R;
import com.dacruzl2.marvelcomics.view.fragment.CharactersFragment;
import com.dacruzl2.marvelcomics.view.fragment.ComicsFragment;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

public class MainActivity extends AppCompatActivity  {

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(),
                FragmentPagerItems.with(this)
                        .add("", CharactersFragment.class)
                        .add("", ComicsFragment.class)
                        .create()
        );

        ViewPager viewPager = findViewById(R.id.viewpager_smart);
        viewPager.setAdapter(adapter);

        SmartTabLayout viewPagerTab = findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(viewPager);


    }

    @Override
    public void onBackPressed(){
        return;
    }

}
