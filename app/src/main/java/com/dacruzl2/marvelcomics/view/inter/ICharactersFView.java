package com.dacruzl2.marvelcomics.view.inter;

import com.dacruzl2.marvelcomics.model.pojoCharacters.Root;

public interface ICharactersFView {


    void hideLoading();

    void showLoading();

    void addNewItemToFeed(Root root);

    void mostrarToast(String mensagem);

    void addItemByModified(Root root);

}
