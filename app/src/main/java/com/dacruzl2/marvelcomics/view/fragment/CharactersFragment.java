package com.dacruzl2.marvelcomics.view.fragment;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.dacruzl2.marvelcomics.R;
import com.dacruzl2.marvelcomics.listener.RecyclerItemClickListener;

import com.dacruzl2.marvelcomics.model.MarvelDAO;
import com.dacruzl2.marvelcomics.model.MarvelDatabase;
import com.dacruzl2.marvelcomics.view.activity.DetailCharactersActivity;
import com.dacruzl2.marvelcomics.view.adapters.CharactersAdapter;
import com.dacruzl2.marvelcomics.model.pojoCharacters.Result;
import com.dacruzl2.marvelcomics.model.pojoCharacters.Root;
import com.dacruzl2.marvelcomics.presenter.impl.CharactersFPresenterImpl;
import com.dacruzl2.marvelcomics.presenter.inter.ICharactersFPresenter;
import com.dacruzl2.marvelcomics.view.inter.ICharactersFView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import id.voela.actrans.AcTrans;

public class CharactersFragment extends Fragment implements ICharactersFView {
    private RecyclerView recyclerViewCharacter;
    private CharactersAdapter charactersAdapter;
    private List<Result> characterList;

    private ProgressBar progressBar;
    private ImageView ivBackgroundRecyclerCharacter;

    private ICharactersFPresenter mICharactersFPresenter;

    public CharactersFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_characters, container, false);

        charactersAdapter = new CharactersAdapter(getActivity(), characterList);

        progressBar = view.findViewById(R.id.progress_characters);
        ivBackgroundRecyclerCharacter = view.findViewById(R.id.ivBackgroundRecyclerCharacter);

        progressBar.getIndeterminateDrawable()
                .setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

        recyclerViewCharacter = view.findViewById(R.id.rvCharactersFragment);
        StaggeredGridLayoutManager layoutManager =
                new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerViewCharacter.setHasFixedSize(true);
        recyclerViewCharacter.setLayoutManager(layoutManager);
        recyclerViewCharacter.setAdapter(charactersAdapter);
        mICharactersFPresenter.configRecyclerScroll(recyclerViewCharacter, getActivity(), layoutManager);
        recyclerViewCharacter.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                recyclerViewCharacter, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                characterList.get(position);

                Intent intent = new Intent(getActivity(), DetailCharactersActivity.class);

                intent.putExtra("descriptionCharacter", characterList.get(position).getDescription());

                intent.putExtra("imagem", characterList.get(position).getThumbnail()
                        .getPath().concat(".") + characterList.get(position).getThumbnail().getExtension());
                intent.putExtra("CharacterName", characterList.get(position).getName());
                startActivity(intent);
                new AcTrans.Builder(getActivity()).performSlideToLeft();
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        }));

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mICharactersFPresenter = new CharactersFPresenterImpl(this);
        characterList = new ArrayList<>();
        setHasOptionsMenu(true);

    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void addNewItemToFeed(Root root) {
        characterList.addAll(root.getData().getResults());
        charactersAdapter.notifyDataSetChanged();

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                MarvelDAO marvelDAO = MarvelDatabase.getInstance(getActivity()).createMarvelDAO();

                marvelDAO.insert(root.getData().getResults());

                return null;
            }

        }.execute();
    }

    @Override
    public void mostrarToast(String mensagem) {

    }

    @Override
    public void addItemByModified(Root root) {
        characterList.clear();
        characterList.addAll(root.getData().getResults());
        charactersAdapter.notifyDataSetChanged();
    }

    @Override
    public void onStart() {
        super.onStart();
        mICharactersFPresenter.onAttachView(this);
        if (characterList.size() == 0) {
            mICharactersFPresenter.retrieveCharacters(getActivity());
        }
    }

    @Override
    public void onStop() {
        mICharactersFPresenter.onDetachView();
        super.onStop();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.orderByModified:

                mICharactersFPresenter.retrieveCharactersByModified(getActivity());
                hideLoading();

                break;
        }
        return super.onOptionsItemSelected(item);

    }
}
