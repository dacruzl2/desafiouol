package com.dacruzl2.marvelcomics.view.inter;

import com.dacruzl2.marvelcomics.model.pojoComics.RootComics;

public interface IComicsFView {
    void hideLoading();

    void showLoading();

    void addNewItemToFeed(RootComics root);

    void mostrarToast(String mensagem);
}
