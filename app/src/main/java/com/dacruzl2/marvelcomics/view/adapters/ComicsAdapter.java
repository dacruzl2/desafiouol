package com.dacruzl2.marvelcomics.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dacruzl2.marvelcomics.R;
import com.dacruzl2.marvelcomics.model.pojoComics.Result;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ComicsAdapter extends RecyclerView.Adapter<ComicsAdapter.ViewHolderComics> {


    private Context context;
    private List<Result> comicsList;

    public ComicsAdapter(Context context, List<Result> comicsList) {
        this.context = context;
        this.comicsList = comicsList;
    }

    @NonNull
    @Override
    public ViewHolderComics onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comics, parent, false);

        return new ComicsAdapter.ViewHolderComics(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderComics holder, int position) {
        Result resultComics = comicsList.get(position);

        holder.tvTitleComics.setText(resultComics.getTitle());

        if (resultComics.getDescription() != null) {
            holder.tvDescriptionComics.setText(resultComics.getDescription().toString());

        } else {
            holder.tvDescriptionComics.setText("No Description");
        }
       Glide.with(context)
                .load(resultComics.getThumbnail().getPath().concat("/portrait_fantastic.")
                        + resultComics.getThumbnail().getExtension())
                .into(holder.ivComics);
    }

    @Override
    public int getItemCount() {
        return comicsList.size();
    }

    class ViewHolderComics extends RecyclerView.ViewHolder {

        ImageView ivComics;
        TextView tvTitleComics, tvDescriptionComics;


        ViewHolderComics(@NonNull View itemView) {
            super(itemView);
            ivComics = itemView.findViewById(R.id.ivComics);
            tvTitleComics = itemView.findViewById(R.id.tvTitleComics);
            tvDescriptionComics = itemView.findViewById(R.id.tvDescriptionComics);

        }
    }
}
