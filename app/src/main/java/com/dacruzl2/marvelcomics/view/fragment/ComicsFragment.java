package com.dacruzl2.marvelcomics.view.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dacruzl2.marvelcomics.R;
import com.dacruzl2.marvelcomics.model.pojoCharacters.Root;
import com.dacruzl2.marvelcomics.model.pojoComics.Result;
import com.dacruzl2.marvelcomics.model.pojoComics.RootComics;
import com.dacruzl2.marvelcomics.presenter.impl.ComicsFPresenterImpl;
import com.dacruzl2.marvelcomics.presenter.inter.IComicsFPresenter;
import com.dacruzl2.marvelcomics.view.adapters.CharactersAdapter;
import com.dacruzl2.marvelcomics.view.adapters.ComicsAdapter;
import com.dacruzl2.marvelcomics.view.inter.IComicsFView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;


public class ComicsFragment extends Fragment implements IComicsFView {


    private IComicsFPresenter mIComicsFPresenter;

    private RecyclerView recyclerViewComics;

    private ComicsAdapter comicsAdapter;

    private List<Result> comicsList;


    public ComicsFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_comics, container, false);

        comicsAdapter = new ComicsAdapter(getActivity(), comicsList);

        recyclerViewComics = view.findViewById(R.id.rvComicsFragment);
        StaggeredGridLayoutManager layoutManager =
                new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        recyclerViewComics.setLayoutManager(layoutManager);
        recyclerViewComics.setHasFixedSize(true);
        recyclerViewComics.setLayoutManager(layoutManager);
        recyclerViewComics.setAdapter(comicsAdapter);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIComicsFPresenter = new ComicsFPresenterImpl(this);
        comicsList = new ArrayList<>();
    }

    @Override
    public void onStart() {
        super.onStart();
        mIComicsFPresenter.onAttachView(this);
        if (comicsList.size() == 0) {
            mIComicsFPresenter.retrieveComics(getActivity());
        }
    }

    @Override
    public void onStop() {
        mIComicsFPresenter.onDetachView();
        super.onStop();
    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void addNewItemToFeed(RootComics root) {
        comicsList.addAll(root.getData().getResults());
        comicsAdapter.notifyDataSetChanged();
    }

    @Override
    public void mostrarToast(String mensagem) {

    }
}
