package com.dacruzl2.marvelcomics.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dacruzl2.marvelcomics.R;
import com.dacruzl2.marvelcomics.model.pojoCharacters.Result;
import com.dacruzl2.marvelcomics.model.pojoCharacters.Root;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;

import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

public class CharactersAdapter extends RecyclerView.Adapter<CharactersAdapter.MyViewHolder> {

    private Context context;
    private List<Result> characterList;

    public CharactersAdapter(Context context, List<Result> characterList) {
        this.context = context;
        this.characterList = characterList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_characters, parent, false);

        return new CharactersAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Result result = characterList.get(position);

        holder.tvNameCharacter.setText(result.getName());
        Glide.with(holder.itemView.getContext())
                .load(result.getThumbnail().getUrl())
                .circleCrop()
                .into(holder.ivCharacter);
    }

    @Override
    public int getItemCount() {
        return characterList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvNameCharacter;
        ImageView ivCharacter;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tvNameCharacter = itemView.findViewById(R.id.tvNameCharacter);
            ivCharacter = itemView.findViewById(R.id.ivCharacter);
        }
    }
}
