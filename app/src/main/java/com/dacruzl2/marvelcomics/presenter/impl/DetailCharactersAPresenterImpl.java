package com.dacruzl2.marvelcomics.presenter.impl;

import com.dacruzl2.marvelcomics.model.impl.DetailCharactersAModelImpl;
import com.dacruzl2.marvelcomics.model.inter.IDetailCharactersAModel;
import com.dacruzl2.marvelcomics.presenter.inter.IDetailCharactersAPresenter;
import com.dacruzl2.marvelcomics.view.inter.IDetailCharactersAView;

public class DetailCharactersAPresenterImpl implements IDetailCharactersAPresenter {
    private IDetailCharactersAView mIDetailCharactersAView;
    private IDetailCharactersAModel mIDetailCharactersAModel;

    public DetailCharactersAPresenterImpl(IDetailCharactersAView aIDetailCharactersAView) {
        mIDetailCharactersAView = aIDetailCharactersAView;
        mIDetailCharactersAModel = new DetailCharactersAModelImpl();
    }
}
