package com.dacruzl2.marvelcomics.presenter.impl;

import android.content.Context;

import com.dacruzl2.marvelcomics.model.Helpers;
import com.dacruzl2.marvelcomics.network.CharactersFModelImpl;
import com.dacruzl2.marvelcomics.model.impl.ComicsFModelImpl;
import com.dacruzl2.marvelcomics.network.ICharactersFModel;
import com.dacruzl2.marvelcomics.model.inter.IComicsFModel;
import com.dacruzl2.marvelcomics.model.pojoComics.RootComics;
import com.dacruzl2.marvelcomics.presenter.inter.IComicsFPresenter;
import com.dacruzl2.marvelcomics.view.inter.IComicsFView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ComicsFPresenterImpl implements IComicsFPresenter {
    private IComicsFView mIComicsFView;
    private IComicsFModel mIComicsFModel;
    private ICharactersFModel mICharactersFModel;

    public ComicsFPresenterImpl(IComicsFView fIComicsFView) {
        mIComicsFView = fIComicsFView;
        mIComicsFModel = new ComicsFModelImpl();
        mICharactersFModel = new CharactersFModelImpl();
    }

    @Override
    public void retrieveComics(Context context) {

        mICharactersFModel.conn(context);
        mICharactersFModel.retrieveComics(100, Helpers.KEY,Helpers.HASH ,1)
                .enqueue(new Callback<RootComics>() {
                    @Override
                    public void onResponse(Call<RootComics> call, Response<RootComics> rootComics) {
                        if (rootComics.isSuccessful()) {

                            mIComicsFView.addNewItemToFeed(rootComics.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<RootComics> call, Throwable t) {

                    }
                });

    }

    @Override
    public void onAttachView(IComicsFView view) {
        mIComicsFView = view;
    }

    @Override
    public void onDetachView() {
        mIComicsFView = null;
    }
}
