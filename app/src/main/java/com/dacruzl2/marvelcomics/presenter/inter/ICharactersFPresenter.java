package com.dacruzl2.marvelcomics.presenter.inter;

import android.content.Context;

import com.dacruzl2.marvelcomics.view.adapters.CharactersAdapter;
import com.dacruzl2.marvelcomics.view.inter.ICharactersFView;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

public interface ICharactersFPresenter {

    void retrieveCharacters(Context context);

    void onAttachView(ICharactersFView view);

    void onDetachView();

    void configRecyclerScroll(RecyclerView recyclerView, Context context,
                              StaggeredGridLayoutManager staggeredGridLayoutManager);

    void retrieveCharactersByModified(Context context);



}
