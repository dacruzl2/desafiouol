package com.dacruzl2.marvelcomics.presenter.impl;

import android.content.Context;
import android.widget.AbsListView;

import com.dacruzl2.marvelcomics.model.Helpers;
import com.dacruzl2.marvelcomics.network.CharactersFModelImpl;
import com.dacruzl2.marvelcomics.network.ICharactersFModel;
import com.dacruzl2.marvelcomics.model.pojoCharacters.Root;
import com.dacruzl2.marvelcomics.presenter.inter.ICharactersFPresenter;
import com.dacruzl2.marvelcomics.view.inter.ICharactersFView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CharactersFPresenterImpl implements ICharactersFPresenter {
    private ICharactersFView mICharactersFView;
    private ICharactersFModel mICharactersFModel;

    private int currentItems;
    private int totalItems;
    private int scrollOutItems;
    private Boolean isScrolling = false;
    char x=65;


    public CharactersFPresenterImpl(ICharactersFView fICharactersFView) {
        mICharactersFView = fICharactersFView;
        mICharactersFModel = new CharactersFModelImpl();
    }

    @Override
    public void retrieveCharacters(Context context) {

        mICharactersFView.showLoading();
        mICharactersFModel.conn(context);
        mICharactersFModel.retrieveCharacters(100, Helpers.KEY, Helpers.HASH, 1,x )
                .enqueue(new Callback<Root>() {
                    @Override
                    public void onResponse(Call<Root> call, Response<Root> root) {
                        x++;
                        if (root.isSuccessful()) {

                            mICharactersFView.addNewItemToFeed(root.body());
                            mICharactersFView.mostrarToast("retrieveCharacters()");
                            mICharactersFView.hideLoading();
                        }
                    }

                    @Override
                    public void onFailure(Call<Root> call, Throwable t) {

                    }
                });

    }

    @Override
    public void onAttachView(ICharactersFView view) {
        mICharactersFView = view;
    }

    @Override
    public void onDetachView() {
        mICharactersFView = null;
    }

    @Override
    public void configRecyclerScroll(RecyclerView recyclerView, Context context,
                                     StaggeredGridLayoutManager staggeredGridLayoutManager) {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = staggeredGridLayoutManager.getChildCount();
                totalItems = staggeredGridLayoutManager.getItemCount();
                scrollOutItems = staggeredGridLayoutManager.findFirstVisibleItemPositions(null)[0];

                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {
                    isScrolling = false;
                    retrieveCharacters(context);
                }
            }
        });
    }

    @Override
    public void retrieveCharactersByModified(Context context) {

        mICharactersFView.showLoading();
        mICharactersFModel.conn(context);
        mICharactersFModel.retrieveCharactersByModified(100,Helpers.KEY, Helpers.HASH, "modified",1)
                .enqueue(new Callback<Root>() {
                    @Override
                    public void onResponse(Call<Root> call, Response<Root> root) {
                        if (root.isSuccessful()) {

                            mICharactersFView.addItemByModified(root.body());

                            mICharactersFView.hideLoading();
                        }
                    }

                    @Override
                    public void onFailure(Call<Root> call, Throwable t) {

                    }
                });

    }
}




