package com.dacruzl2.marvelcomics.presenter.inter;

import android.content.Context;

import com.dacruzl2.marvelcomics.view.inter.ICharactersFView;
import com.dacruzl2.marvelcomics.view.inter.IComicsFView;

public interface IComicsFPresenter {

    void retrieveComics(Context context);

    void onAttachView(IComicsFView view);

    void onDetachView();
}
