package com.dacruzl2.marvelcomics.network;

import android.content.Context;

import com.dacruzl2.marvelcomics.model.pojoCharacters.Root;
import com.dacruzl2.marvelcomics.model.pojoComics.RootComics;

import retrofit2.Call;

public interface ICharactersFModel {

    Call<Root> retrieveCharacters(int limit, String apikey, String hash, long ts, char c);

    Call<RootComics> retrieveComics(int limit, String apikey, String hash, long ts);

    Call<Root> retrieveCharactersByModified(int limit,String apikey, String hash, String orderBy, long ts);

    void conn(Context context);
}
