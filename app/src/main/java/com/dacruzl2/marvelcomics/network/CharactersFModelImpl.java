package com.dacruzl2.marvelcomics.network;

import android.content.Context;

import com.dacruzl2.marvelcomics.model.Helpers;
import com.dacruzl2.marvelcomics.model.pojoCharacters.Root;
import com.dacruzl2.marvelcomics.model.pojoComics.RootComics;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class CharactersFModelImpl implements ICharactersFModel {

    private MarvelService marvelService;

    private static final Interceptor REWRITE_CACHE_CONTROL_INTERCEPTOR = chain -> {
        Response originalResponse = chain.proceed(chain.request());
        return originalResponse.newBuilder()
                .removeHeader("Pragma")
                .header("Cache-Control",
                        String.format("max-age=%d", 60))
                .build();
    };

    @Override
    public Call<Root> retrieveCharacters(int limit, String apikey, String hash, long ts, char c) {
        return marvelService.retrieveCharacters(limit, ts, apikey,c ,hash);
    }

    @Override
    public Call<RootComics> retrieveComics(int limit, String apikey, String hash, long ts) {
        return marvelService.retrieveComics(limit,apikey, ts, hash );
    }

    @Override
    public Call<Root> retrieveCharactersByModified(int limit, String apikey, String hash, String orderBy, long ts) {
        return marvelService.retrieveCharactersByModified(apikey,hash,ts,limit,orderBy);
    }


    public void conn(Context context) {

        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        File httpCacheDirectory = new File(context.getCacheDir(), "offlineCache");

        //10 MB
        Cache cache = new Cache(httpCacheDirectory, 10 * 1024 * 1024);

        OkHttpClient httpClient = new OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor(httpLoggingInterceptor)
                .addNetworkInterceptor(REWRITE_CACHE_CONTROL_INTERCEPTOR)
                .addInterceptor(provideOfflineCacheInterceptor())
                .addInterceptor(provideCacheInterceptor())
                .addInterceptor(new StethoInterceptor())

                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .client(httpClient)
                .baseUrl(Helpers.BASE_URL)
                .build();

        marvelService = retrofit.create(MarvelService.class);

    }

    public static Interceptor provideCacheInterceptor() {

        return chain -> {
            Request request = chain.request();
            Response originalResponse = chain.proceed(request);
            String cacheControl = originalResponse.header("Cache-Control");

            if (cacheControl == null || cacheControl.contains("no-store") || cacheControl.contains("no-cache") ||
                    cacheControl.contains("must-revalidate") || cacheControl.contains("max-stale=0")) {


                CacheControl cc = new CacheControl.Builder()
                        .maxStale(1, TimeUnit.DAYS)
                        .build();

                    /*return originalResponse.newBuilder()
                            .header("Cache-Control", "public, max-stale=" + 60 * 60 * 24)
                            .build();*/


                request = request.newBuilder()
                        .cacheControl(cc)
                        .build();

                return chain.proceed(request);

            } else {
                return originalResponse;
            }
        };

    }


    public static Interceptor provideOfflineCacheInterceptor() {

        return chain -> {
            try {
                return chain.proceed(chain.request());
            } catch (Exception e) {


                CacheControl cacheControl = new CacheControl.Builder()
                        .onlyIfCached()
                        .maxStale(1, TimeUnit.DAYS)
                        .build();
                Request offlineRequest = chain.request().newBuilder()
                        .cacheControl(cacheControl)
                        .header("Cache-Control", "public, only-if-cached")
                        .removeHeader("Pragma")
                        .build();
                return chain.proceed(offlineRequest);
            }
        };
    }

}



