package com.dacruzl2.marvelcomics.network;

import com.dacruzl2.marvelcomics.model.pojoCharacters.Root;
import com.dacruzl2.marvelcomics.model.pojoComics.RootComics;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MarvelService {


    @GET("characters")
    Call<Root> retrieveCharacters(@Query("limit") int limit,
                                  @Query("ts") long ts,
                                  @Query("apikey") String apikey,
                                  @Query("nameStartsWith") char c,
                                  @Query("hash") String hash);

    @GET("comics")
    Call<RootComics> retrieveComics(@Query("limit") int limit,
                                    @Query("apikey") String apikey,
                                    @Query("ts") long ts,
                                    @Query("hash") String hash);

    @GET("characters")
    Call<Root> retrieveCharactersByModified(@Query("apikey") String apikey,
                                            @Query("hash") String hash,
                                            @Query("ts") long ts,
                                            @Query("limit") int limit,
                                            @Query("orderBy") String orderBy);
}
